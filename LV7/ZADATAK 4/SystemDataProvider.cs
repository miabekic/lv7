﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_4
{
    class SystemDataProvider:SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (previousCPULoad - currentLoad >= previousCPULoad / 10 || currentLoad - previousCPULoad >= previousCPULoad / 10) 
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentAvailabeRAM = this.AvailableRAM;
            if (previousRAMAvailable - currentAvailabeRAM >= previousRAMAvailable / 10 || currentAvailabeRAM - previousRAMAvailable >= previousRAMAvailable / 10)  
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentAvailabeRAM;
            return currentAvailabeRAM;
        }
    }
}

