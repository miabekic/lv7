﻿using System;

namespace ZADATAK_4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleLogger logger = new ConsoleLogger();
            SystemDataProvider dataProvider = new SystemDataProvider();
            dataProvider.Attach(logger);
            while (true)
            {
                dataProvider.GetAvailableRAM();
                dataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
