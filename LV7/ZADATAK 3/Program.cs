﻿using System;

namespace ZADATAK_3
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleLogger logger = new ConsoleLogger();
            SystemDataProvider dataProvider = new SystemDataProvider();
            dataProvider.Attach(logger);
            while (true)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
