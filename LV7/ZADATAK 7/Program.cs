﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ZADATAK_7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IItem> items = new List<IItem>();
            Book book = new Book("Hunger games", 150.21);
            DVD dvd1 = new DVD("Microsoft", DVDType.SOFTWARE, 502.15);
            VHS vhs = new VHS("New colection", 12.14);
            DVD dvd2 = new DVD("Aladin", DVDType.MOVIE, 70.15);
            IVisitor rentVisitor = new RentVisitor();
            IVisitor buyVisitor = new BuyVisitor();
            Cart cart = new Cart(items);
            cart.AddItem(book);
            cart.AddItem(dvd1);
            cart.AddItem(vhs);
            cart.AddItem(dvd2);
            Console.WriteLine("Iznajmljivanje: "+cart.Accept(rentVisitor)+"kn\nKupovina: "+ cart.Accept(buyVisitor)+"kn\n");
            cart.RemoveItem(dvd1);
            Console.WriteLine("Iznajmljivanje: " + cart.Accept(rentVisitor) + "kn\nKupovina: " + cart.Accept(buyVisitor) + "kn");
        }
    }
}
