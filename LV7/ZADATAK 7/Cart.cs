﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class Cart
    {
        private IList<IItem> items = new List<IItem>();
        public Cart(IList<IItem> items)
        {
            this.items = items;
        } 
        public void AddItem(IItem item)
        {
            items.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            items.Remove(item);
        }
        public double Accept(IVisitor visitor)
        {
            double price = 0;
            foreach(IItem item in items)
            {
                price+=item.Accept(visitor);
            }
            return price;
        }
    }
}
