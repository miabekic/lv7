﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class RentVisitor:IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            IVisitor buyVisitor = new BuyVisitor();
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return buyVisitor.Visit(DVDItem);
            }
            else
            {
                return DVDItem.Price / 10;
            }
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price / 10;
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price / 10;
        }
    }
}
