﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
