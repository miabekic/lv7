﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_6
{
    class RentVisitor:IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }
            else
            {
                return DVDItem.Price / 10;
            }
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price / 10;
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price / 10;
        }
    }
}
