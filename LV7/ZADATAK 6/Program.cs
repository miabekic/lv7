﻿using System;

namespace ZADATAK_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Hunger games", 150.21);
            DVD dvd = new DVD("Microsoft", DVDType.SOFTWARE,502.15);
            VHS vhs = new VHS("New colection", 12.14);
            IVisitor rentVisitor = new RentVisitor();
            book.Accept(rentVisitor);
            dvd.Accept(rentVisitor);
            vhs.Accept(rentVisitor);
            Console.WriteLine(rentVisitor.Visit(book) + "\n" + rentVisitor.Visit(dvd) + "\n" + rentVisitor.Visit(vhs));
        }
    }
}
