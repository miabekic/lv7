﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_5
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
