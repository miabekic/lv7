﻿using System;

namespace ZADATAK_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Hunger games", 150.25);
            DVD dvd = new DVD("Moana", DVDType.MOVIE, 100.15);
            VHS vhs = new VHS("Vjencanje", 75.10);
            IVisitor visitor = new BuyVisitor();
            Console.WriteLine(book.Accept(visitor));
            Console.WriteLine(dvd.Accept(visitor));
            Console.WriteLine(vhs.Accept(visitor));
        }
    }
}
