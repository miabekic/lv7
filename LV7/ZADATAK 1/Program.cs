﻿using System;

namespace ZADATAK_1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 5.12, 8.4, 1.5, 10.1, -1, 6.02, -3.47, 15, 1.64, 10.15 };
            NumberSequence numberSequence1 = new NumberSequence(array);
            NumberSequence numberSequence2 = new NumberSequence(array);
            NumberSequence numberSequence3 = new NumberSequence(array);
            SortStrategy sequentialSort = new SequentialSort();
            SortStrategy combSort = new CombSort();
            SortStrategy bubbleSort = new BubbleSort();
            numberSequence1.SetSortStrategy(sequentialSort);
            numberSequence1.Sort();
            Console.Write("Sekvencijalno sortiranje:\n" + numberSequence1);
            numberSequence2.SetSortStrategy(combSort);
            numberSequence2.Sort();
            Console.WriteLine("\nKombinirano sortiranje:\n" + numberSequence2);
            numberSequence3.SetSortStrategy(bubbleSort);
            numberSequence3.Sort();
            Console.WriteLine("\nBubble sort:\n" + numberSequence3);
        }
    }
}
