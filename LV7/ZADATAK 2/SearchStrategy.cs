﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_2
{
    interface SearchStrategy
    {
        public bool Search(double[] array, double number);
    }
}
