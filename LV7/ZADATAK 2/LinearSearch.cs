﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_2
{
    class LinearSearch:SearchStrategy
    {
        public bool Search(double[] array, double number)
        {
            bool numberFound = false;
            for(int i = 0; i < array.Length; i++)
            {
                if (number == array[i])
                {
                    numberFound = true;
                }
            }
            return numberFound;
        }
    }
}
