﻿using System;

namespace ZADATAK_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 1.7, 5.2, 10.8, -5.5, 0, 5.7, 6.9, -14.5, 11.01, 21.7 };
            NumberSequence numberSequence = new NumberSequence(array);
            SearchStrategy linearSearch = new LinearSearch();
            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine(numberSequence.Search(5.7));
        }
    }
}
